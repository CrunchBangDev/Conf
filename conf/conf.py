import argparse
import os
import sys
import textwrap
from collections.abc import Iterable
from typing import Any, Dict, Optional, Tuple, Union

import yaml

from conf import ConfDict, ConfItem

important = "moo"
YAY, NAY = ("y", "Y"), ("n", "N")


class Conf:
  __slots__ = 'abstract_conf', 'conf', 'conf_file', '__dict__'

  def __init__(self,
               abstract_conf: tuple,
               filename: str = "settings.yml",
               prog: str = "Conf",
               version: str = "",
               env_prefix: str = None,
               export: Optional[bool] = False,
               test: Optional[bool] = False,
               **kwargs: Any) -> None:
    """
    Build conf from available sources

    >>> Conf([], test=True)
    {}
    """
    self.conf_file = filename
    self.abstract_conf = abstract_conf
    self.test = test

    env_prefix = env_prefix or ''.join(char for char in prog if char.isupper())

    conf = ConfDict()

    # Load stored conf
    if os.path.isfile(filename):
      conf.update(yaml.safe_load(open(filename)) or {})

    # Convert imported values into ConfItem objects
    for conf_item in abstract_conf:
      try:
        conf[conf_item.name] = conf_item(conf[conf_item.name])
      # Failed conversion to ConfItem
      except (ValueError, TypeError):
        conf.pop(conf_item.name)
      # Item not present
      except KeyError:
        continue

    # Remove unrecognized imported keys from conf
    for k, v in conf.copy().items():
      if k not in (x.name for x in abstract_conf):
        print(f"Rejecting illegal property '{k}': {conf.pop(k)}")

    # Get commandline input with argparse
    cli_args = self.init_argparse(prog, version, **kwargs)

    # Process commandline arguments
    conf.update(self.cli_input(cli_args))

    # Process environment variables
    conf.update(self.env_input(env_prefix))

    # If conf is default, offer first time wizard
    _export = None
    if not conf and getattr(sys.stdout, 'isatty', bool)():
      wizard_conf, _export = self.wizard()
      if wizard_conf:
        conf.update(wizard_conf)

    export = export or _export

    # Fill in unspecified options with defaults
    conf.update({x.name: x for x in abstract_conf
                 if x.name not in conf.keys()})

    # Export if specified
    if export:
      self.save(conf, filename)

    # All done!
    self.config = conf
  
  def save(self, conf: dict, filename: str = "settings.yml") -> None:
    yaml.safe_dump({k: v.value for k, v in conf.items()},
                   open(filename, 'w'))

  def init_argparse(self,
                    prog: str,
                    version: str,
                    description: str = "",
                    **kwargs: Any) -> dict:
    """
    >>> Conf((ConfItem('moo', 'moo', str, 'moo'), \
              ConfItem('-mooo', 'moo', str, ['moo']), \
              ConfItem('--moooo', 'moo')), \
             test=True).init_argparse('test', '') \
                # doctest:+ELLIPSIS
    {...}
    """
    # Populate argument parser
    parser = argparse.ArgumentParser(
      formatter_class=argparse.RawDescriptionHelpFormatter,
      description=f"{prog} v{version}{os.linesep}{description}",
      epilog=textwrap.dedent("""\
      Any flags set at runtime will override the imported settings.
      Environment variables will override all other settings.
      """),
      **kwargs
    )
    for item in (*self.abstract_conf, ConfItem(f"--{important}")):
      if item.flag[:2] == "--":
        parser.add_argument(item.flag,
                            help=item.help,
                            required=False,
                            action="store_true",
                            dest=item.name)
      elif item.flag[:1] == "-":
        parser.add_argument(item.flag,
                            help=item.help,
                            required=False,
                            type=item.type,
                            nargs="+",
                            dest=item.name)
      else:
        parser.add_argument(item.name,
                            help=item.help,
                            type=item.type,
                            nargs="?")

    try:
      args = parser.parse_args().__dict__
    except SystemExit:  # wtf
      if not self.test:
        raise
      args = {}

    args.get(important, None) and (print(
      ' ' * 10,
      "{}\n  {}\n   {}\n   {}\n{}\n{}\n {}\n   {}\n   {}\nthe  {}\nc"
      f"{important[-1]}w   {{}}\nsays    {{}}\n'{important}'    {{}}"
      f"\n{' '*8}{{}}\n{' '*7}{{}}\n     {{}}\n{' '*11}{{}}".format(
        ",=    ,        =.", ("_  _   /'/    )\\,/,/(_   \\`\\"),
        ("`//-.|  (  ,\\\\)\\//\\)\\/_  ) |"), ("//___\\   `\\\\\\/\\"
        "\\/\\/\\\\///'  /"), (",-\"~`-._ `\"--'_   `\"\"\"`  _ \\`'"
        "\"~-,_"), ("\\       `-.  '_`.      .'_` \\ ,-\"~`/"), ("`."
        "__.-'`/   (-\\        /-) |-.__,'"), ("||   |     \\O)  /^"
        "\\ (O/  |"), ("`\\\\  |         /   `\\    /"), ("\\\\  \\ "
        "      /      `\\ /"), ("`\\\\ `-.  /' .---.--.\\"), ("`\\\\"
        "/`~(, '()      ()"), "/(O) \\\\   _,.-.,_)", "//  \\\\ `\\'"
        "`      /", "/ |  ||   `\"\"~~~\"`", "/'  |__||", "`o")
    ) or exit(0))  # type: ignore
    return args

  def cli_input(self, args: dict) -> dict:
    """
    >>> Conf((ConfItem('test'), ), test=True).cli_input({'test': 'test'})
    {'test': test}
    """
    conf = {}
    for item in self.abstract_conf:
      cli_option = args.get(item.name, None)
      if cli_option in (None, False):
        continue
      if isinstance(cli_option, Iterable) and not isinstance(cli_option, str):
        processed_cli_option = []
        for option in cli_option:
          processed_cli_option.extend("".join(option).split(","))
        cli_option = processed_cli_option
      conf[item.name] = item(cli_option)
    return conf

  def env_input(self, prefix: str = None) -> dict:
    """
    >>> Conf((ConfItem('test'), ), test=True).env_input('test')
    {}
    """
    conf = {}
    env = os.environ
    for item in self.abstract_conf:
      name = f"{f'{prefix}_' if prefix else ''}{item.name.upper()}".upper()
      env_option = env.get(name, None)
      if env_option is None:
        continue
      conf[item.name] = item(env_option)
    return conf

  def wizard(self) -> Tuple[Optional[Dict], Optional[bool]]:
    """
    >>> import io, sys; sys.stdin = io.StringIO('no')
    >>> Conf([], test=True).wizard()
    Welcome to Conf
    (None, None)
    """
    print('Welcome to Conf')
    wizard_choice = ConfItem('--wizard',
                             default=True)
    if self.abstract_conf:
      try:
        wiz = self.get_bool_choice(wizard_choice,
                                   'Would you like to run the setup wizard?')
        if wiz.value:
          conf = {}
          for item in self.abstract_conf:
            conf.update({item.name: self.present_wizard_option(item)})
          exp = f"Would you like to save these options to {self.conf_file}?"
          export_decision = self.get_bool_choice(ConfItem('--export'), exp)
          return conf, export_decision.value
      except Exception:
        print(f"You have slain the wizard!")
        raise
    return None, None

  def present_wizard_option(self, item: ConfItem) -> \
          Optional[Union[ConfItem, str]]:
    """
    >>> import io, sys; sys.stdin = io.StringIO('no')
    >>> Conf([], test=True).present_wizard_option(ConfItem('--test', 'test'))
    Test: test [y]/[N]: False
    """
    choice: Optional[Union[ConfItem, str]] = None
    item_help = item.wizard or item.help
    item_text = ' '.join(item.name.split('_')).title() + ": " + item_help
    while not isinstance(choice, ConfItem):
      if item.dashes == 2 or item.type is bool:  # item is boolean flag
        choice = self.get_bool_choice(item, item_text)
      elif item.dashes == 1:  # item is compound argument
        choice = self.get_compound_choice(item, item_text)
      else:  # item is normal argument
        choice = self.get_choice(item, item_text)
    return choice or None

  def get_bool_choice(self, item: ConfItem, item_text: str) -> ConfItem:
    """
    >>> import io, sys; sys.stdin = io.StringIO('no')
    >>> Conf([], test=True).get_bool_choice(ConfItem('--test'), 'test')
    test [y]/[N]: False
    """
    if item.default:
      choice = input(f"{item_text} [Y]/[n]: ")
      choice_result = item(False if choice and choice[0] in NAY else True)
    else:
      choice = input(f"{item_text} [y]/[N]: ")
      choice_result = item(True if choice and choice[0] in YAY else False)
    return choice_result

  def get_choice(self, item: ConfItem, item_text: str) -> Optional[ConfItem]:
    """
    >>> import io, sys; sys.stdin = io.StringIO('no')
    >>> Conf([], test=True).get_choice(ConfItem('test', default='test'), 'hi')
    hi (blank for default: test): no
    """
    default = ""
    if item.default:
      default = f" (blank for default: {item.default})"
    choice = input(f"{item_text}{default}: ")
    if not len(choice):
      choice_result = item.default
    else:
      choice_result = item.type(choice)
    try:
      return item(choice_result)
    except (ValueError, TypeError) as e:
      print(f"Sorry, that value is invalid: {e}")
      raise

  def get_compound_choice(self, item: ConfItem, item_text: str) -> ConfItem:
    """
    >>> import io, sys; sys.stdin = io.StringIO(chr(10))
    >>> Conf([], test=True).get_compound_choice( \
          ConfItem('-test', '', str, default=['test']), '')
    <BLANKLINE>
    Enter a value (blank for default: ['test']): []
    """
    list_complete = False
    value_list: Any = []
    print(item_text)
    while not list_complete:
      prompt_suffix = ('(blank when done)' if len(value_list) else
                       f'(blank for default: {item.default})') + ": "
      list_item = input(f"Enter a value {prompt_suffix}")
      if list_item in ("", "*"):
        choice = item(value_list)
        list_complete = True
      elif item.allowed and list_item not in item.allowed:
        print("Sorry, that's not one of the allowed values: "
              ', '.join(item.allowed))
      else:
        value_list.append(list_item)
    # Apply default
    if choice is None:
      choice = item
    return choice

  def __repr__(self) -> str:
    return str(self.config)


if __name__ == '__main__':
  import doctest
  doctest.testmod()
