import doctest
import importlib
import os
import sys
import unittest


def load_tests(loader, tests, ignore):  # type: ignore
  """ Finds doctests in child modules and returns them for unittest to run """
  # Get working directory
  path, _ = os.path.realpath(__file__).rsplit(os.path.sep, 1)
  # Add package path to system path to avoid import errors
  sys.path.insert(1, __package__)
  # Look for tests in each Python file discovered
  for filename in os.listdir(path):
    name, ext = os.path.splitext(filename)
    # Skip non-Python and dunder files
    if not ext == '.py' or name[:2] == '__':
      continue
    module = importlib.import_module(name, __package__)
    tests.addTests(doctest.DocTestSuite(module))
  return tests


unittest.main()
