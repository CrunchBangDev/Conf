from typing import Any

from conf import ConfItem


class ConfDict(dict):
  """
  A dictionary designed to store ConfItem objects

  >>> assert(isinstance(ConfDict(), dict))
  """
  def get(self, key: str, default: Any = None) -> Any:
    """
    >>> assert(ConfDict({'tkey': 'tval'}).get('tkey') == 'tval')
    """
    item = super().get(key, default)
    return self.property_as_value(item)

  def __getitem__(self, key: str, raw: bool = False) -> Any:
    """
    >>> assert(ConfDict({'tkey': 'tval'})['tkey'] == 'tval')
    """
    item = super().__getitem__(key)
    if raw:
      return item
    return self.property_as_value(item)

  def __setitem__(self, key: str, value: Any) -> None:
    """
    >>> x = ConfDict({'tkey': 5}); x['tkey'] = 1; x['tkey']
    1
    """
    current = self.__getitem__(key, True)
    if isinstance(current, ConfItem):
      value = current(value)
    super().__setitem__(key, value)

  @staticmethod
  def property_as_value(prop: ConfItem) -> Any:
    """
    Handles conversion of ConfItems to their values

    >>> assert(ConfDict.property_as_value('test') == 'test')
    """
    return getattr(prop, 'value', prop)


if __name__ == '__main__':
  import doctest
  doctest.testmod()
