from __future__ import annotations

import argparse
import uuid
from collections.abc import Iterable
from typing import Any, Callable as Callable, Type, Optional

# Create a random default value for use when None is a valid argument value
DEFAULT = uuid.uuid4()
t = type

class ConfItem:
  """
  A configuration item intended to be stored in a ConfDict

  >>> ConfItem('--test', default=True)
  True
  """
  __slots__ = ('help', 'default', 'allowed', 'required', 'check', 'wizard',
               'flag', '_name', '_type', '_value')

  def __init__(self,
               name: str,
               help: str = "",
               type: Optional[type] = None,
               default: Any = DEFAULT,
               allowed: tuple = None,
               required: bool = False,
               check: Callable = None,
               wizard: str = None,
               **kwargs: dict) -> None:
    """
    >>> ConfItem('--test')
    False
    """
    self.name = name
    self.help = help or argparse.SUPPRESS
    if self.dashes == 2:
      type = bool
    if type is not None:
      if default == DEFAULT:
        default = type()
    elif default != DEFAULT:
      type = t(default)
    self.type = type or getattr(self, '_type', None) or str
    self.default = default
    self.allowed = allowed
    self.required = required
    self.check = check
    self.wizard = wizard or help

  @property
  def name(self) -> str:
    """
    >>> assert(ConfItem('-').name == '')
    """
    return self._name

  @name.setter
  def name(self, name: str) -> None:
    """
    >>> x = ConfItem('--test').name = 'test'; print(x)
    test
    """
    self.flag = name
    self._name = name.strip('-')

  @property
  def dashes(self) -> int:
    """
    >>> assert(ConfItem('--test').dashes == 2)
    >>> assert(ConfItem('-test').dashes == 1)
    >>> assert(ConfItem('test').dashes == 0)
    """
    return len(self.flag) - len(self.name)

  @property
  def type(self) -> Type:
    """
    >>> assert(isinstance(ConfItem('--test').type, type))
    """
    return self._type

  @type.setter
  def type(self, ntype: Type) -> None:
    """
    >>> x = ConfItem('--test').type = str; print(x)
    test
    """
    if not isinstance(ntype, type):
      raise TypeError(f"Type must be {type} not {type(ntype)}")
    self._type = ntype

  @property
  def value(self) -> Any:
    """
    >>> ConfItem('test').value = 'test'
    """
    if getattr(self, '_value', DEFAULT) != DEFAULT:
      return self._value
    if self.default != DEFAULT:
      return self.default
    if isinstance(self.type, type):
      return self.type()

  @value.setter
  def value(self, value: Any) -> None:
    if self.dashes == 1:
      if not isinstance(value, Iterable):
        raise TypeError(f"Value must be iterable not {type(value)}")
      values = []
      for sub_value in value:
        sub_value = self.coerce_value(sub_value)
        self.check_value(sub_value)
        values.append(sub_value)
      value = values
    else:
      value = self.coerce_value(value)
      self.check_value(value)
    self._value = value

  def coerce_value(self, value: Any) -> Any:
    """
    >>> ConfItem('--test').coerce_value('test')
    True
    >>> ConfItem('-test').coerce_value('test')
    'test'
    """
    if self.type and not isinstance(value, self.type):
      # Accept defaults that don't conform to type
      if value == self.default and not value == DEFAULT:
        return value
      value = self.type(value)
    return value

  def check_value(self, value: Any) -> None:
    """
    >>> ConfItem('--test').check_value(True)
    >>> ConfItem('-test').check_value(['test'])
    >>> ConfItem('test').check_value('test')
    """
    if self.check and not bool(self.check(value)):
      raise ValueError(f"Value failed validation: {value}: {self.check}")
    if self.allowed and value not in self.allowed:
      raise ValueError(f"Value not in allowed values: {value}: {self.allowed}")
    if self.type and not self.typecheck(value):
      raise TypeError(f"Value must be {self.type} not {type(value)}: {value}")

  def typecheck(self, value: Any) -> bool:
    """
    >>> ConfItem('--test').typecheck(True)
    True
    >>> ConfItem('-test').typecheck(['test'])
    True
    >>> ConfItem('test').typecheck('test')
    True
    >>> ConfItem('-test').typecheck(True)
    False
    >>> ConfItem('test').typecheck(['test'])
    False
    >>> ConfItem('--test').typecheck('test')
    False
    """
    if isinstance(value, Iterable) and self.dashes == 1:
      for sub_value in value:
        if not isinstance(sub_value, self.type):
          return False
    else:
      if not isinstance(value, self.type):
        return False
    return True

  def __repr__(self) -> str:
    """
    >>> str(ConfItem('test', default='test'))
    'test'
    """
    return str(self.value)

  def __call__(self, value: Any) -> ConfItem:
    """
    >>> ConfItem('--test')('test')
    True
    """
    self.value = value
    return self


if __name__ == '__main__':
  import doctest
  doctest.testmod()
