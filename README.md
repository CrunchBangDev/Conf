# Conf

A kind of strongly typed config wrangler

## Setup

```sh
pip install git+https://gitlab.com/CrunchBangDev/conf.git
```

## Usage

Initialize the `Conf` class with a collection of `ConfItem`s:

```python
from conf import Conf, ConfItem

config = Conf(
  (
    ConfItem('string', 'a string', str, 'string'),
    ConfItem('integer', 'an integer', int, 5),
    ConfItem('-list', 'a list of strings', str, ['hello']),
    ConfItem('--bool', 'a boolean'),  # Type is inferred
  )
).config
```

Conf will then check to see whether you have a `settings.yml` file, or passed
in any of these flags through the commandline, or whether you've added
variables to the environment with matching keys.

When searching for settings in the environment or a settings file, the dashes
are dropped from the setting name. For environment variables, in order to
discourage clobbering items in the global environment, Conf will prepend a
string and an underscore; this string defaults to a collection of all capital
letters in the calling script's name.

The `ConfDict` object created by the above code will behave like a dictionary:

```python
>>> config['string']
'string'
>>> config['string'] = 'my new string'
>>> config['string']
'my new string'
```

...but behind the scenes, its contents are still `ConfItem`s. You can access
the full item by sending `True` as a second argument to `__getitem__()`:

```python
>>> type(config.__getitem__('string', True))
<class 'conf.conf_item.ConfItem'>
```

This is useful because the validation capabilities of `ConfItem` are retained:
```python
>>> config['integer'] = 'test'
Traceback (most recent call last):
  ...
ValueError: invalid literal for int() with base 10: 'test'
```

On the other hand, if the `ConfItem` can easily coerce a value into its desired
type, it will do so rather than complain:
```python
>>> config['string'] = 42
>>> config['string']
'42'
```

Note that looping over `config.items()` exposes the underlying `ConfItem` as
well.
